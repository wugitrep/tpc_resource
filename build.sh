ROOT_DIR=$(pwd);
# nodejs
export NODE_HOME=$ROOT_DIR/node-v16.15.0-linux-x64;
export PATH=$PATH:$NODE_HOME;
echo "**********nodejs**********";
if [ ! -d "$NODE_HOME" ]; then
   wget https://nodejs.org/download/release/v16.15.0/node-v16.15.0-linux-x64.tar.gz;
   tar -xvf node-v16.15.0-linux-x64.tar.gz;
fi  
chmod 777 $NODE_HOME/bin/node;
chmod 777 $NODE_HOME/bin/npm;
npm config set @ohos:registry=https://repo.harmonyos.com/npm/;
npm config set lockfile=false;

# ohpm
echo "********ohpm********";
OHPM_FILE_DIR=$ROOT_DIR/tpc_resource/ohpm;
export PATH=$PATH:$OHPM_FILE_DIR/bin;
cd $OHPM_FILE_DIR/bin;
chmod 777 init;
./init;
cd $ROOT_DIR;

# sdk
echo "**********sdk*********";
export OHOS_BASE_SDK_HOME=$(pwd)/ohos-sdk/linux;
export OHOS_SDK_HOME=$(pwd)/ohos-sdk/linux;
export PATH=$PATH:$OHOS_BASE_SDK_HOME;
export PATH=$PATH:$OHOS_SDK_HOME;
if [ ! -d "$OHOS_BASE_SDK_HOME" ]; then
   wget https://mirrors.huaweicloud.com/openharmony/os/3.2.2/ohos-sdk-windows_linux-public.tar.gz;
   tar -xvf ohos-sdk-windows_linux-public.tar.gz;
   for file in $(ls $OHOS_BASE_SDK_HOME)
   do
	   if [ -f $OHOS_BASE_SDK_HOME/$file ] 
	   then
	      cd $OHOS_BASE_SDK_HOME;
	      unzip $file;
	      cd $ROOT_DIR;
           fi
   done
   cd $OHOS_BASE_SDK_HOME;
   cd ets/build-tools/ets-loader;
   npm install;
   cd $OHOS_BASE_SDK_HOME;
   cd js/build-tools/ace-loader;
   npm install;
   cd $OHOS_BASE_SDK_HOME;
   mkdir 9;
   mv ets 9;
   mv native 9;
   mv js 9; 
   mv toolchains 9;
   cd $ROOT_DIR;  
else
   cd $OHOS_BASE_SDK_HOME/9;
   cd ets/build-tools/ets-loader;
   npm install;
   cd $OHOS_BASE_SDK_HOME/9;
   cd js/build-tools/ace-loader;
   npm install;
   cd $ROOT_DIR;
fi

# Hap存放目录
HAP_FILE_DIR="$(pwd)/out/tpc";
if [ ! -d "$HAP_FILE_DIR" ]; then
mkdir -p out/tpc;
fi

# PR地址
PR_URL=$1;
echo "*************pr***************"$PR_URL;
if [[ $PR_URL == *openharmony-tpc* ]]
then
   echo "***********openharmony-tpc**************";
   PR_LEFT=${PR_URL#*openharmony-tpc/};
else
   echo "***********openharmony-sig**************";
   PR_LEFT=${PR_URL#*openharmony-sig/};
fi   
COMPONT_NAME=${PR_LEFT%/pulls*};
cd $COMPONT_NAME;
CURWORK_DIR=$(pwd);
if [ $COMPONT_NAME == "openharmony_tpc_samples" ]; then
   echo "*************openharmony_tpc_samples***************";	
   exit 0;
fi   

# compile
echo "***********compile************";
cd $CURWORK_DIR;
if [[ ! -e "$CURWORK_DIR/oh-package.json5" ]] && [[  ! -e "$CURWORK_DIR/package.json" ]]; then
   exit 0;	
fi	
if [ -e "$CURWORK_DIR/oh-package.json5" ]; then
   for path in $(find . -name "oh-package.json5")
   do
      path_array=(${path//\// });
      lenth=${#path_array[@]};
      if [ $lenth == 3 ]; then
	 echo "*******ohpm install*********";
         cd ${path_array[1]};
	 ohpm install;
	 cd $CURWORK_DIR;
      fi 
   done
   cd $CURWORK_DIR;   
   ohpm install;
   chmod 777 hvigorw;
   ./hvigorw --mode module assembleHap --no-daemon;
else
    cd $CURWORK_DIR;
    for path in $(find . -name "package.json")
     do
       path_array=(${path//\// });
       lenth=${#path_array[@]};       
       if [ $lenth == 3 ]; then
	  echo "*******npm install*********";	
	  cd ${path_array[1]}; 
	  npm install;
	  cd $CURWORK_DIR;
      fi
    done      
    cd $CURWORK_DIR;
    npm install;
    $NODE_HOME/bin/node $CURWORK_DIR/node_modules/@ohos/hvigor/bin/hvigor.js --mode module assembleHap;
fi    
if [ $? -eq 0 ]; then
  echo "************compile success****************";	
  #拷贝Hap
  cp -rf $CURWORK_DIR/entry/build/default/outputs $HAP_FILE_DIR;
else
  exit 1
fi
